/**/
#include <omp.h>
#include <vector>
#include <list>
#include <set>

// for testing only.
#include <chrono>
#include <ctime>

#include <iostream>
using namespace std;
#include <bits/stdc++.h>
#include "structures.h"
#include "chain.h"
#include "site.h"
#include "pattern.h"
#include "utils.h"

// parse each protein. return a vector with all Chains.
void parses_pdbs_version1(vector<string> pdbs, parameters params, vector<Chain> &all_chains) {
    cout << "parsing pdbs... " << endl;
    //auto start = chrono::system_clock::now();
    
    // number of PDBs.
    int n = pdbs.size();
    // store chains per thread.
    vector<Chain> chains_thread[params.nthreads];
    
    // parses groups of PDBs in parallel.
    #pragma omp parallel num_threads(params.nthreads)
    {
        // thread ID.
        int id = omp_get_thread_num();
        
        #pragma omp for schedule(runtime)
        for (int i=0; i<n; i++) {
#ifdef TRACING
Extrae_event(100, i+1);
#endif
            
            // get Chains from pdbs[i].
            vector<Chain> tmp = parse_pdb(pdbs[i], params);
            chains_thread[id].insert(chains_thread[id].end(), tmp.begin(), tmp.end());
#ifdef TRACING
Extrae_event(100, 0);
#endif
        }
    }

    // (sequential) insert the chains of each thread in all_chains[].
    for (int id=0; id<params.nthreads; id++) {
#ifdef TRACING
Extrae_event(101, id+1);
#endif
        all_chains.insert(all_chains.end(), chains_thread[id].begin(), chains_thread[id].end());
#ifdef TRACING
Extrae_event(101, 0);
#endif
    }
}

void parses_pdbs_version2(vector<string> pdbs, parameters params, vector<Chain> &all_chains) {
    cout << "parsing pdbs... " << endl;
    //auto start = chrono::system_clock::now();
    
    // number of PDBs.
    int n = pdbs.size();
    
    // store chains per thread.
    vector<Chain> chains_thread[params.nthreads];
    
    // parses groups of PDBs in parallel.
    #pragma omp parallel num_threads(params.nthreads)
    {
        // thread ID.
        int id = omp_get_thread_num();
        
        #pragma omp for nowait schedule(runtime)
        for (int i=0; i<n; i++) {
#ifdef TRACING
Extrae_event(100, i+1);
#endif
            // get Chains from pdbs[i].
            vector<Chain> tmp = parse_pdb(pdbs[i], params);
            chains_thread[id].insert(chains_thread[id].end(), tmp.begin(), tmp.end());
#ifdef TRACING
Extrae_event(100, 0);
#endif
        }

        // reduction
        // insert the chains of each thread in all_chains[].
        #pragma omp critical
        {
#ifdef TRACING
Extrae_event(101, id+1);
#endif
            all_chains.insert(all_chains.end(), chains_thread[id].begin(), chains_thread[id].end());
#ifdef TRACING
Extrae_event(101, 0);
#endif
        }
    }
}

// process chains.
void process_chains_version1(parameters params, vector<Chain> &all_chains, vector<Pattern> &all_patterns) {
    cout << "processing chains... " << endl;
    //auto start = chrono::system_clock::now();
    
    // number of Chains.
    int n_chains = all_chains.size();
    
    // store patterns per thread.
    vector<Pattern> patterns_thread[params.nthreads];
    
    #pragma omp parallel num_threads(params.nthreads)
    {
        // thread ID.
        int id = omp_get_thread_num();
        
        #pragma omp for schedule(runtime)
        for (int c=0; c<n_chains; c++) {
#ifdef TRACING
Extrae_event(200, c+1);
#endif
            create_kdtree(&all_chains[c]);
            calc_geom_center_of_residues(&all_chains[c]);
            calc_max_min_coordinates(&all_chains[c]);
            find_sites(&all_chains[c], params);
            // patterns in this chain.
            merge_patterns_in_chain(&all_chains[c], params);
            patterns_thread[id].insert(patterns_thread[id].end(), 
                all_chains[c].patterns.begin(), 
                all_chains[c].patterns.end());
#ifdef TRACING
Extrae_event(200, 0);
#endif
        }
    }

    // (sequential) merge patterns into all_patterns[].
    for (int id=0; id<params.nthreads; id++) {
#ifdef TRACING
Extrae_event(201, id+1);
#endif
        merge_all_patterns(patterns_thread[id], all_patterns);
#ifdef TRACING
Extrae_event(201, 0);
#endif
        //
        //printf("thread: %d n_local_patterns: %d\n", id, patterns_thread[id].size());
    }
}

/**/
void process_chains_version2(parameters params, vector<Chain> &all_chains, set<Pattern> *set_patterns) {
    cout << "processing chains... " << endl;
    int n_chains = all_chains.size();
    vector<Pattern> patterns_thread[params.nthreads];
        
    #pragma omp parallel num_threads(params.nthreads)
    {
        int id = omp_get_thread_num();
        
        //
        long unsigned int n_patterns;
        std::pair<std::set<Pattern>::iterator,bool> ret;
        std::set<Pattern>::iterator iter;
        
        #pragma omp for nowait schedule(runtime)
        for (int c=0; c<n_chains; c++) {
#ifdef TRACING
Extrae_event(200, c+1);
#endif
            create_kdtree(&all_chains[c]);
            calc_geom_center_of_residues(&all_chains[c]);
            calc_max_min_coordinates(&all_chains[c]);
            find_sites(&all_chains[c], params);
            merge_patterns_in_chain(&all_chains[c], params);
            patterns_thread[id].insert(patterns_thread[id].end(), 
                all_chains[c].patterns.begin(), 
                all_chains[c].patterns.end());
#ifdef TRACING
Extrae_event(200, 0);
#endif
        }
        
        // reduction. 
        // merge patterns into all_patterns[].
        #pragma omp critical
        {
#ifdef TRACING
Extrae_event(201, id+1);
#endif
            //
            n_patterns = patterns_thread[id].size();
            
            //
            //printf("thread: %d n_local_patterns: %d\n", id, n_patterns);
            
            for (long unsigned int p=0; p<n_patterns; p++) {
                ret = set_patterns->insert(patterns_thread[id][p]);
                iter = ret.first;
                
                // false == already exist the pattern true == new pattern.
                if (ret.second == false) { 
                    // pattern found. adds sites to the patterns.
                    iter->sites.insert(iter->sites.end(), 
                        patterns_thread[id][p].sites.begin(), 
                        patterns_thread[id][p].sites.end()
                    );
                    
                    // adds proteins to patterns.
                    iter->in_protein.insert(iter->in_protein.end(), 
                        patterns_thread[id][p].in_protein.begin(), 
                        patterns_thread[id][p].in_protein.end()
                    );
                }
            }
#ifdef TRACING
Extrae_event(201, 0);
#endif
        }
    }
}

// filter the patterns applying the coverture percentage.
void filter_patterns_version1(parameters params, vector<string> pdbs, vector<Pattern> &all_patterns, vector<Pattern> &f_patterns) {
    printf("filtering the patterns: %d ...\n", all_patterns.size());

    vector<Pattern> f_patterns_thread[params.nthreads];
    int n_patterns = all_patterns.size();
    
    #pragma omp parallel num_threads(params.nthreads)
    {
        //
        int id = omp_get_thread_num();
        
        #pragma omp for schedule(runtime)
        for (int i=0; i<n_patterns; i++) {
            
#ifdef TRACING
Extrae_event(300, i+1);
#endif
            sort(all_patterns[i].in_protein.begin(), all_patterns[i].in_protein.end());
            all_patterns[i].in_protein.erase(
                unique(all_patterns[i].in_protein.begin(), all_patterns[i].in_protein.end()), 
                all_patterns[i].in_protein.end()
            );
            
            // calculate pattern coverage.
            float pattern_coverage = ((float)all_patterns[i].in_protein.size()/(int)pdbs.size())*100;
            
            // erase pattern.
            if (pattern_coverage >= params.coverage) {
                all_patterns[i].coverage = pattern_coverage;
                cout << all_patterns[i].name << " " << all_patterns[i].coverage << endl;
                
                f_patterns_thread[id].push_back(all_patterns[i]);
                //f_patterns.push_back(all_patterns[i]);
            }
            
#ifdef TRACING
Extrae_event(300, 0);
#endif

        }
    }
    
    //
    for (int id=0; id<params.nthreads; id++) {
        f_patterns.insert(f_patterns.end(), f_patterns_thread[id].begin(), f_patterns_thread[id].end());
    }
    
    //
    printf("filtered patterns: %d ...\n", f_patterns.size());
}

/*
void filter_patterns_version1(parameters params, vector<string> pdbs, vector<Pattern> &all_patterns) {
    //auto start = chrono::system_clock::now();
    printf("filtering the patterns: %d ...\n", all_patterns.size());
        
    int coverage = params.coverage;
    int pdbssize = pdbs.size();
    vector<Pattern>::iterator new_end;

#ifdef TRACING
Extrae_event(300, 1);
#endif

    new_end = remove_if(
        all_patterns.begin(),
        all_patterns.end(),
        [pdbssize, coverage](Pattern &p) {

            // remove duplicates proteins in the patterns.
            sort(p.in_protein.begin(), p.in_protein.end());
            p.in_protein.erase(
                unique(p.in_protein.begin(), p.in_protein.end()), 
                p.in_protein.end()
            );

            // calculate pattern coverage.
            float pattern_coverage = ((float)p.in_protein.size()/(int)pdbssize)*100;
            p.coverage = pattern_coverage;
            
            if (coverage < pattern_coverage) \
                cout << p.name << " " << p.coverage << endl;

            return pattern_coverage < coverage;
        }
    );

    // update vector size.
    all_patterns.erase(new_end, all_patterns.end());
#ifdef TRACING
Extrae_event(300, 0);
#endif
}
*/

//
// https://stackoverflow.com/questions/2513988/iteration-through-std-containers-in-openmp
void filter_patterns_version2(parameters params, vector<string> pdbs, set<Pattern> &set_patterns, set<Pattern> &f_patterns) {
    printf("filtering the patterns: %d ...\n", set_patterns.size());

#ifdef TRACING
int i=0;
#endif

    #pragma omp parallel num_threads(params.nthreads) 
    {
        for (set<Pattern>::iterator it = set_patterns.begin(); it != set_patterns.end(); ++it) {
#ifdef TRACING
Extrae_event(300, i+1);
#endif
            #pragma omp single nowait
            {


                sort(it->in_protein.begin(), it->in_protein.end());
                it->in_protein.erase(
                    unique(it->in_protein.begin(), it->in_protein.end()), 
                    it->in_protein.end()
                );
                
                // calculate pattern coverage.
                float pattern_coverage = ((float)it->in_protein.size()/(int)pdbs.size())*100;
                
                //
                if (pattern_coverage >= params.coverage) {
                    it->coverage = pattern_coverage;
                    #pragma omp critical
                    {
                        cout << it->name << " " << it->coverage << endl;
                        f_patterns.insert(*it);
                    }
                }
            }
#ifdef TRACING
i++;
Extrae_event(300, 0);
#endif
        }
    }
    
    //
    printf("filtered patterns: %d ...\n", f_patterns.size());
}

//
int main(int argc, char **argv) {
    auto start_t = chrono::system_clock::now();
    
    //FIXME: validate parameters.
    parameters params;
    params.step = atof(argv[1]);
    params.radius = atof(argv[2]);
    params.rmsd = atof(argv[3]);
    params.coverage = atof(argv[4]);
    params.extend = atof(argv[5]);
    params.nres = atof(argv[6]);
    params.nthreads = atof(argv[7]);
    params.pdb_list_file = argv[8];
    params.dirpdbs = argv[9];
    params.dirout = argv[10];
  
    //FIXME: remove duplicates ids.
    // read list of pdb files.
    vector<string> pdbs = gets_pdbs_id_from_file(params.pdb_list_file);
    
    //
#ifdef VERSION1
    // vector to store all Patterns with its Clusters and Sites.
    vector<Pattern> all_patterns;
    // vector to store all Chains.
    vector<Chain> all_chains;
    // parses pdb files.
    parses_pdbs_version1(pdbs, params, all_chains);
    // process chains.
    process_chains_version1(params, all_chains, all_patterns);
        
#elif VERSION2
    set<Pattern> set_patterns;
    // vector to store all Chains.
    vector<Chain> all_chains;
    // parses pdb files.
    parses_pdbs_version2(pdbs, params, all_chains);
    // process chains.
    process_chains_version2(params, all_chains, &set_patterns);
#endif
        
    // filtering patterns.
#if defined(VERSION2)
    set<Pattern> f_patterns;
    filter_patterns_version2(params, pdbs, set_patterns, f_patterns);
    //filter_patterns_version2(params, pdbs, set_patterns);
#else
    vector<Pattern> f_patterns;
    filter_patterns_version1(params, pdbs, all_patterns, f_patterns);
    //filter_patterns_version1(params, pdbs, all_patterns);
#endif

    // clustering sites.
#if defined(VERSION2)
    //cluster_sites_version2(params, set_patterns);
    cluster_sites_version2(params, f_patterns);
#else
    cluster_sites_version1(params, f_patterns);
    //cluster_sites_version1(params, all_patterns);
#endif

    /*
    // save results.
#if defined(VERSION2)
    save_data_to_folder_version2(f_patterns, params, pdbs);
#else
    save_data_to_folder_version1(f_patterns, params, pdbs);
#endif
        
    save_parameters(params, pdbs);
    */
    
    cout << "clustered sites... " << endl;
    //
    auto end_t = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = end_t-start_t;
    cout << "total time: " << elapsed_seconds.count() << " seconds" << endl;
    
    exit(0);
}
