prefix=/usr/local
CC = g++
kdlib = kdtree/libkdtree.a

# VV= 1,2
VV=2

# normal compilation.
CFLAGS = -DVERSION$(VV) -O3 -std=c++11 -fopenmp -march=native -pipe 
LDFLAGS = $(kdlib) -lm

# Instrumentation framework -lomptrace (extrae)
#CFLAGS = -I$(EXTRAE_HOME)/include/ -DTRACING -DVERSION$(VV) -O3 -std=c++11 -fopenmp -march=native -pipe 
#LDFLAGS = $(kdlib) -lm -L$(EXTRAE_HOME)/lib/ -lomptrace 

SRC = 3d-pp.cpp chain.cpp residue.cpp atom.cpp site.cpp pattern.cpp rmsd.cpp utils.cpp
OBJ = 3d-pp.o chain.o residue.o atom.o site.o pattern.o rmsd.o utils.o
APP = 3d-pp$(VV)

all: $(OBJ)
	$(CC) $(CFLAGS) -o $(APP) $(OBJ) $(LIBS) $(LDFLAGS)

%.o: %.cpp
	$(CC) $(CFLAGS) -c $<

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
