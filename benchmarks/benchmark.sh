#!/bin/bash
cd ..
times=3
total_cpus=25
schedule=static
#
ofile="benchmarks/data.txt"
etime="benchmarks/elapsed_times.txt"

# delete old data.
rm $etime 2>/dev/null
rm $ofile 2>/dev/null


export OMP_SCHEDULE=$schedule

# run command.
for ncpus in $(seq 2 1 $total_cpus)
do
    # delete old data.
    rm $ofile 2>/dev/null
    
    echo "#cpus: " $ncpus
    
    # run command.
    for (( counter=1; counter<=$times; counter++ ))
    do
        echo "test: " $counter
        /usr/bin/time --output=$ofile --format="%e" -a ./3d-pp 0.8 3 5 80 0 4 $ncpus list-46.txt PDBExample/ results/;
    done
    
    # read results.
    sum=0
    while IFS= read -r line
    do
        sum=$(bc -l <<<"$sum+$line")
        echo "elapsed time: " $line
    done < "$ofile"

    # calculate the average.
    avg=$(bc -l <<<"$sum/$times")
    echo "sum: " $sum
    echo "average: " $avg
    
    echo "-----------------"
    
    #echo "cpu:$ncpus:etime:$avg" >> $etime
    echo "$ncpus,$avg" >> $etime
done

#cd benchmarks/

#python3 plot.py elapsed_times.txt
