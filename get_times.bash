#!/bin/bash

total_cpus=64
schedule=static,1

for ncpus in $(seq 2 2 $total_cpus)
do
    cat power9_openmp_$schedule-$ncpus.out | egrep total | awk -F" " {' print $3 '}
done
