#!/bin/bash
# sbatch job.cmd

#SBATCH --job-name="8-OpenMP"
#SBATCH -D .
#SBATCH --output=power9_openmp_weak-8.out
#SBATCH --error=power9_openmp_weak-8.err
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --nodes=1
#SBATCH --time=00:05:00

module load gcc/10.1.0
export OMP_SCHEDULE=dynamic; ./3d-pp 0.8 3 5.0 80 0 4 8 list-8.txt PDBExample/ /dev/null
