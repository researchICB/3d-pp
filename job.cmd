#!/bin/bash
# sbatch job.cmd

#SBATCH --job-name="omp-3d-pp"
#SBATCH -D .
#SBATCH --output=openmp_%j.out
#SBATCH --error=openmp_%j.err
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --nodes=1
#SBATCH --time=00:05:00
###SBATCH --mem=0

module load gcc/10.1.0
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_SCHEDULE="dynamic"

./3d-pp 0.8 3 5 80 0 4 $SLURM_CPUS_PER_TASK list-46.txt PDBExample/ results/

