#!/usr/bin/env python

###from os import write
import subprocess
import os

# 
total_tasks = 25
#
SCHE = "static"
###SCHE = "static,1"
###SCHE = "dynamic"
#
listp = "list-46.txt"
#
pdbs = "PDBExample/"
#
results = "results/"

# create scripts.
for tasks in range(2, total_tasks + 1, 1):
    os.environ['OMP_SCHEDULE'] = SCHE
    os.environ['OMP_NUM_THREADS'] = str(tasks)
    subprocess.call(["./3d-pp", "0.8", "3", "5.0", "80", "0", "4", str(tasks), listp, pdbs, results])
