#!/usr/bin/env python

"""
$ python run_script_power9.py
"""

import subprocess

#
scripts = []

#
wall_clock_limit = "00:06:00"

# 
t_threads = 64

#
schedule = "dynamic"

# create scripts.
for threads in range(2, t_threads+2, 2):
    script="""#!/bin/bash
# sbatch power9_openmp_{threads}.cmd

#SBATCH --job-name="{threads}_{schedule}-openmp-3dpp"
#SBATCH -D .
#SBATCH --output=power9_openmp_{schedule}-{threads}.out
#SBATCH --error=power9_openmp_{schedule}-{threads}.err
#SBATCH --time={wall_clock_limit}
#SBATCH --ntasks=1
#SBATCH --cpus-per-task={threads}
#SBATCH --nodes=1
###SBATCH --mem=0

module load gcc/10.1.0
export OMP_SCHEDULE={schedule}; ./3d-pp 0.8 3 5.0 80 0 4 {threads} list-46.txt PDBExample/ results

""".format(threads=threads, wall_clock_limit=wall_clock_limit, schedule=schedule)
    
    # writes to file.
    filename = "power9_openmp_" + schedule + "-" + str(threads) + ".cmd"
    f = open(filename,"w")
    f.write(script)
    f.close()

    scripts.append(filename)

# run jobs
for s in scripts:
    subprocess.call(["sbatch", s])    # python 2.7
    ###subprocess.run(["sbatch", s])  # python 3.
