#!/bin/bash

total_cpus=64
schedule=static

for ncpus in $(seq 2 2 $total_cpus)
do
        echo "threads: " $ncpus
        export OMP_SCHEDULE=$schedule; ./3d-pp 0.8 3 5.0 80 0 4 $ncpus list-46.txt PDBExample/ /dev/null
        echo "-------------------------"
done
