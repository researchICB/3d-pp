#!/bin/bash

#SBATCH --job-name="extrae_openmp-3d-pp"
#SBATCH -D .
#SBATCH --output=extrae_power9_openmp.out
#SBATCH --error=extrae_power9_openmp.err
#SBATCH --time=01:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=160
#SBATCH --nodes=1
###SBATCH --mem=0

###module load gcc/10.1.0
###module load EXTRAE/4.0.1

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_SCHEDULE="dynamic"

###bash run_ldpreload-extrae.sh ../3d-pp-v2 0.8 3 5.0 80 0 4 $SLURM_CPUS_PER_TASK ../list-46.txt ../PDBExample/ ../results/

bash run_ldpreload-extrae.sh ../3d-pp-v2 0.8 3 5.0 70 0 4 $SLURM_CPUS_PER_TASK ../list-8344.txt ../pdbs_dpocket_fda2/ ../results_fda2/
