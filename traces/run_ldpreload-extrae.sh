#!/bin/bash

# on power9
source $EXTRAE_HOME/etc/extrae.sh

export EXTRAE_CONFIG_FILE=extrae.xml
export LD_PRELOAD=${EXTRAE_HOME}/lib/libomptrace.so

# run the program
$*
