#!/bin/bash
export EXTRAE_ON=1

export OMP_SCHEDULE="dynamic"
export OMP_DISPLAY_ENV=true
export OMP_DISPLAY_AFFINITY=true
export OMP_AFFINITY_FORMAT='level %L thread %n affinity %A host %H'
export OMP_PLACES="cores"
export OMP_PROC_BIND="spread"

# local PC
source /usr/local/etc/extrae.sh

# on power9
##source $EXTRAE_HOME/etc/extrae.sh

###export EXTRAE_CONFIG_FILE=traces/extrae.xml
export EXTRAE_CONFIG_FILE=extrae.xml
export LD_PRELOAD=${EXTRAE_HOME}/lib/libomptrace.so 

$*
